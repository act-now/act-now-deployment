# act now! deployment

## Clone the repository

In order to clone the repository, you must create a GitLab
[Deploy token](https://docs.gitlab.com/ee/user/project/deploy_tokens/).

Create a new group level deploy token with the following permissions:

- `read_repository`
- `read_registry`

Then, you can clone the repository with the following commands:

```bash
# Store the Git credentials - https://git-scm.com/docs/git-credential-store
git config --global credential.helper store

# Clone the repository
git clone https://gitlab.com/act-now/act-now-deployment.git
```

Any further operations will use the credentials stored in the previous step
(`~/.git-credentials`).

## Log in to the GitLab container registry

In order to pull the Docker images from the container registry, you must create
a GitLab Deploy token. You can use the same token as before.

Then, you can log in to the GitLab container registry with the following
command:

```bash
# Log in to the GitLab container registry
docker login registry.gitlab.com
```

Any further operations will use the credentials stored in the previous step
(`~/.docker/config.json`).

## Start the applications

Check the following README for instructions on how to start the applications:

- [act now!](act-now/README.md)
- [Traefik](traefik/README.md)
- [WikiMedia](wikimedia/README.md)

## Free up space

To free up space, you can delete the unused Docker containers and volumes with
the following command:

```bash
# Delete all the stopped containers and volumes
docker system prune --all
```

You can check the remaining disk space with the following command:

```bash
# Check the remaining disk space
df --human-readable
```
